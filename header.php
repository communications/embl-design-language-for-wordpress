<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package EBI Visual Framework
 * @since EBI Visual Framework 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php wp_head(); ?>

    <!-- <meta name="ebi:masthead-color" content="#11180f" />
    <meta name="ebi:masthead-image" content="//tsc.ebi.ac.uk/sites/tsc.ebi.ac.uk/files/tsc/images/sections/tsc-about-us.png" /> -->
  </head>
  <body <?php body_class(); ?>>
  <div id="skip-to">
    <a href="#content">Skip to main content</a>
  </div>

  <header class="clearfix masthead-black-bar">
    <div class="row">
      <div class="columns small-12">
        <a href="http://www.embl.org" class="no-underline"><img src="https://v4-tag-springboard-grp-stratcom.surge.sh/images/embl-logo.svg" alt="EMBL Logo" class="embl-logo"/>
          <span class="show-for-medium">European Molecular Biology Laboratory</span>
        </a>

        <span class="r-al"></span>
      </div>

      <div class="columns medium-8 hide">
        <ul class="menu float-right no-underline small">
          <li>
          <a target="_parent" href="https://www.embl.org/" title="Explore">Explore</a>
          </li>
          <li>
          <a target="_parent" href="https://intranet.embl.de/" title="Sign in">Sign in</a>
          </li>
          <li>
          <a target="_parent" href="/communications-gantry/search" title="Search">Search</a>
          </li>
        </ul>
      </div>

    </div>
  </header>

  <?php do_action( 'ebiframework_after_body' ); ?>

  <?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
  <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
  <?php endif; ?>

  <?php do_action( 'ebiframework_layout_start' ); ?>

  <div>
    <header id="masthead" class="masthead">
      <div class="masthead-inner row">
        <!-- local-title -->
        <div class="local-title columns medium-8" id="local-title">
          <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
          <!-- <?php bloginfo( 'description' ); ?> -->
        </div>
        <!-- /local-title -->

        <div class="columns medium-4">
          <?php dynamic_sidebar( 'header-widgets' ); ?>
        </div>

      </div>
    </header>
  </div>

  <div class="clear menu-wrapper">
    <!-- local-nav -->
    <nav id="main-menu" class="row navigation" role="navigation">
      <?php ebiframework_top_bar_r(); ?>
      <?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
        <?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
      <?php endif; ?>
    </nav>
    <!-- /local-nav -->
  </div>

  <div id="content" class="row padding-top-large" role="main">

    <section class="container">
      <?php do_action( 'ebiframework_after_header' );
