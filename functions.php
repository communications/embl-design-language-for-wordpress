<?php
	function project_dequeue_unnecessary_styles() {
		wp_dequeue_style( 'theme-stylesheet' );
		wp_deregister_style( 'theme-stylesheet' );
		wp_dequeue_style( 'ebi-stylesheet' );
		wp_deregister_style( 'ebi-stylesheet' );
	}
	add_action( 'wp_print_styles', 'project_dequeue_unnecessary_styles' );


	 function embl_design_language_for_wordpress_enqueue_styles() {
		 wp_enqueue_style( 'vf-stylesheet', 'https://ebi.emblstatic.net/web_guidelines/EBI-Framework/v1.3/css/ebi-global.css', array(), '1.3', 'all' );
		 wp_enqueue_style( 'theme-stylesheet-embl', 'https://master-branch-embl-visual-framework-layer-grp-stratcom.surge.sh/css/theme-embl.css', array(), '1.0', 'all' );
		 wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
		 wp_enqueue_style( 'local-style', get_stylesheet_directory_uri() . '/style.css' );
	 }
	 add_action( 'wp_enqueue_scripts', 'embl_design_language_for_wordpress_enqueue_styles' );

   // remove `.tag` from body on tag pages
   add_filter('body_class', function (array $classes) {
    if (in_array('tag', $classes)) {
      unset( $classes[array_search('tag', $classes)] );
    }
    return $classes;
  });

 ?>
